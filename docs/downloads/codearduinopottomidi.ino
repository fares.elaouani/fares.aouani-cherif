#define knob A0
#define led 3

void setup() { 
  pinMode(knob, INPUT);
  Serial.begin(9600);
}

void loop() {
  //Read potentimeter input value
  int value = analogRead(knob);
  value = map(value, 0, 1023, 0, 127);
  Serial.print("Pot : ");
  Serial.println(value);
  delay(100);
}

 
