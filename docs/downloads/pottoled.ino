#define knob 0
#define led 3


void setup() {
  pinMode(led, OUTPUT);  

}

void loop() {
  
  int val = analogRead(knob);
  val = map(val, 1, 1024, 1, 255);
  analogWrite(led, val);
}
