# About me



 Hi ! I am Fares from Carthago in Tunisia. The city was first "created" around 814 Before Christ by a woman called Elyssa Didon. The legend says, she went to the King and asked him for a piece of land the size of a cow's skin. The king laughed at her and accepted. 
Elyssa was very intelligent, so she cut the cow's skin into one really thin thread and surrounded a huge piece of land with it. The king was really angry but since he accepted the deal, he had to give her that land.

At around 100 B.c. came Hannibal, son of Amilcar, a very famous Warrior. At that time Carthago was the "center" of the world along with Rome. Rome and Carthago were in the middle of the Mediterranean Sea and all the trading went through those two cities. They were really powerful and rich.

Fast forward 2000 years later i was born. Now I study Medien- und Kommunikationsinformatik at Hochschule Rhein-Waal. I'm in the 5th semester and i am really interested in the design part of things. I like designing websites, prototypes, 3D-modelling, animation and even coding.

![](../images/avatar-photo.jpeg)

## Previous work

When I was 13, I downloaded "Cinema 4D" from Maxon and started to learn how to create 3D models. I then created a Youtube Channel where i uploaded some "intros" for gamers and some frag movies of me playing a game called "Counter-Strike 1.6".

![](../images/youtubechannel.png)

