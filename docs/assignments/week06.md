# 5. Embedded Programming

This week I worked on my very first arduino. The task was to read any human input and do something with it.

## Problem I got

The arduino isnt detected by my computer. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/fail.png"  width="300"/>
<br>
After a little bit of research in google, i found out that I had to install the CH34x driver. After downloading the driver and installing it, I tried again but it still didnt work.
I then found some more informations on google that i had to instalkl the FDTI Driver for the new versions of Mac OSx which I did, but still in vain.
After a long research and failed attempts I found an article on the forum of apple that said that the arduino with the FDTI chip can't be detected by my Catalina version, but it also said that the problems was solved...

## Temporary Solution 
Since the arduino just isn't detected by my computer no matter what I did. So for this assignment I worked with my college "Tolga Avci" on his computer so I can code and try my code.

## Idea
So I decided to use the monitor as the input device. The user just has to give a number between 1 and 9 and the arduino reads it. After reading the input 

``` 
byte ledPin=13;
long x = 0;
char receiveData;
int n;
void setup() {
  pinMode (ledPin, OUTPUT);
  Serial.begin(9600);
  Serial.println("write a number from 1-9 to turn the LED on for that amount of seconds: ");
}
void loop() {
  if(Serial.available() > 0){
    receiveData = Serial.read();
    n = receiveData - '0';
    if(n >=1 && n <= 9){
      x=n*1000;
      Serial.print("led on for: ");
      Serial.print(n);
      Serial.print(" seconds");
      digitalWrite(ledPin, HIGH);
      delay(x);
      digitalWrite(ledPin, LOW);
      delay(1000);
      Serial.println();
    }else if(n<1 || n>9){
    Serial.println("try again");
     }
     else{
    Serial.println("try again");
     }
   }
}
```

## Tests

After executing the code the user is asked to input a number between 1 and 9 : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/input.png"  width="400"/>
<br>
After the input :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/afterinput.png"  width="400"/>
<br>
Arduino light turns on for the amount of seconds that were inputted :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/off.jpg"  width="300"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/turnson.jpg"  width="300"/>
<br>

As you can see in the pictures, the code works and the light turns on and off depending on the input.

## Download the files :

Code :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/codearduino.ino)

