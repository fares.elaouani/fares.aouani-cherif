# 1. Project management

## My website

This week I got started on using Git. I did set up a page to document about my progress.


### Designing the website

  **Markdown Syntax I used**
  
![](../images/commands.png)

### Downloading, installing and creating a Markdown website

-Download Git
-add Git username : <br> 
 $ Git config –global user.name “Fares.Aouani-Cherif”

-Configure email adress for uploading :<br>
  $ Git config –global user.email”fares.elaouani@gmail.com”

-Check if there is an SSH Key already<br>
  $ cat ~/.ssh/id_rsa.pub 

-Generate SSH key<br>
  $ ssh-keygen -t rsa -C “fares.elaouani@gmail.com”

-Keygen<br>
  $ cat ~/.ssh/id_rsa.pub

-Copy the Key

-Add the copied key to GIT on the web version

-Create a Markdown website

To upload the work that is saved offline, open terminal and go to the repository in the computer.
Here are the main operations to do when you want to push your work:

![](../images/git.png)





