# 2. 2D and 3D Design

This week i worked on my 2D and 3D Design. For this, I used LibreCAD and Fusion.

## First 2D sketch

First, i used LibreCAD for the 2D sketch.
This is the first idea I had. But i wasnt happy with it. Something was disturbing me in it's design.
<img src="http://fares.elaouani.gitlab.io/fares.aouani-cherif/images/libreCAD2d.png"  width="400"/>

## Process and designing : 2D Design

I created two upper splines like so : 
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/splines2d.png"  width="400"/>

<br>
and two lower splines with open edge like so :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/lowersplines.png"  width="400"/>
<br>
I joined the edges with a line : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/line.png"  width="400"/>
<br>
and then drew a circle and an ellipse for the thumb :
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/final.png"  width="400"/>
<br>
## 3D sketch

I then used Fusion from autodesk to create the 3D version. As you can see, my model looks a bit different. I removed the corners on the sides and made the device a bit bigger, so it doesnt break easily.
Plus, I modeled it so, that it looks a bit simpler and more interesting.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/fusion3d1.png"  width="500"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/fusion3d2.png"  width="500"/>

<br>
## Process and designing : 3D Design

I thought of creating a little "device" that one can use as a book-holder. The hole in the middle is there for the thumb to be stuck in, then you phold the book open with only your thumb in the middle, thanks to the "device", it is easy to keep the both pages of the book wide open without making any effort or using a second hand. The second little hole is for the keychain.

I used two splines. two lines and two circles to create my model. It is really simple, but useful.

**First spline**
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/spline1.png"  width="400"/>
<br>
**second spline**
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/spline2.png"  width="400"/>
<br>
**whole model**
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/everything.png"  width="400"/>
<br>

Then I used "Extrude" to make the model in 3D.
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/extrudeshape.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/extrudedshape.png"  width="400"/>
<br>

I then drew two circles and extruded them. Now I got the hole in the middle for the thumb
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/1fusion3D.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/2fusion3D.png"  width="400"/>
<br>




## Download the files :

2D LibreCAD DXF Format : 
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/Bookmark_Sketch_-_1.dxf)

3D Fusion f3d Format :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/Bookmark_-_1.f3d)


