# Final Project

## Problem I have encountered

I unfortunately encountered a problem, which is that the parts and sensors always lose their soldered cables, so at the end of my project I was left with only one potentiometer and one button that were fully connectable to my arduino. I tried my best anyway to work with what I have.
Another problem is that the potentiometer that is fully wired is different than the ones taht the designed knobs fit on, so in the final video, one can not see the 3d printed knobs on the potentiometer.

## Presentation of the project

### Idea

I produce mainly techno music since 6 years now, and I have a lot of midi devices at home. They are very expensive tho and I always wanted to create my own midi device that suits my needs, so as a final project I could finally make that dream come true. 
### Parts 

My midi device should have 11 potentiometers: Five of them would be addressed to the master track so I can contorl the whole song, and the rest should control avrious instruments. I am also going to use an encoder potentiometer to create multiple pages so my potentiometers can have multiple functions. Also, I am adding buttons to turn on and off some effects and finally, two pressure sensors that I will map on filters, flangers and delays. This is really exciting to me because it will enable me to do whatever I want with less effort.

## Creation

### Phase one: The box

After drawing many sketches on paper, I chose one design and I recreated it on fusion360. The material thickness of the box is 6mm except of the top that should have 3mm (so the parts fit). The design didnt have to be parametric.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/boxfusion.png"  width="400"/>
<br>

### Phase two: The knobs

After being done with the box, my next step was to 3D print the knobs that I will put on the potentiometers. This is how the knobs look like :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/knobs.png"  width="400"/>
<br>

### Phase three : Coding

The potentiometers give out an analog input that goes from 0 to 1024. A midi value goes from 0 to 127, so I had to find a way to translate the read value to a midi value. The solution was pretty easy, I just had to map the analogRead value as so : 
```
potCState[i] = analogRead(POT_ARDUINO_PIN[i]); // reads the pins from arduino

    midiCState[i] = map(potCState[i], 0, 1023, 0, 127);
```
The buttons also get a value that is either 0 or 127, that would mean on and off (since I want to use the buttons as interrupters for the effects)

I found a video on youtube that was perfect for me, because it helped me do exactly what I wanted, the guy explained everything very well and one of the main reasons I managed to "complete" my final project. 
This is the link of the video : [link](https://www.youtube.com/watch?v=nh7J71ItDn8)

The full code is relatively long, so I will put it here to download (at the bottom of the page).

## Execution 

After writing the code, the potentiometer and the button work perfectly, they give a correct midi value but the DAW (Digital Audio Workstation) cannot read the value, since it isn't translated from Serial to midi yet. I found a software called "Hairless" that does that job for me. I had a problem downloading it though. The software doesnt work on the new updates of the macbook since it is encoded in 32bits, but after a small research I found a github page of a student who developped an image to make this work. I was so happy I found that because it would otherwise. mean that I can't finish my project because all the softwares I found on internet that translate a value to midi are either for Windows or encoded in 32bits. 
<br>
Here is a picture of the program :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/hairless.png"  width="400"/>
<br>
on the left side, I have to choose the Serial port. In my case it's the arduino Mega. On the right side, i choose the mixing bus that gets the midi output signal. To do that I had to go to the utilities in my macbook and create a port on bus1, as shown in this picture : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/midimac.png"  width="400"/>
<br>
Now I can add the midi output and my next step is to open my DAW and try it out !

## Result

On this picture, you can see how I chose the arduino as my main midi controller. Now I am ready to perform ! with only one potentiometer and one button .. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/ableton.png"  width="400"/>
<br>

### Live Test

Here is the live video :
[link](https://youtu.be/MolNNm1JSAU)




## Future work

Well as you can see, the project is not complete, but the code is there for everything that still has to come. My future work would be to solder everything propperly, and connect everything and it will work.

## Download the files :

Box design 3D Fusion f3d Format :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/3D_Knight_v2.f3d)

Turn Knob for potentiometer in STL format :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/Turn_knob.STL)

Arduino code :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/finalproject.ino)


