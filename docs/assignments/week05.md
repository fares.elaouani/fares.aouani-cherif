# 4. 3D Printing

This week i worked on my 3D Design, that I would be later on printing in 3D. For this, I used Fusion360.
I thought of designing a chess piece : a knight. My name "Fares" means "knight" in Arabic. This is not the reason why i wanted to print that piece in particular, but I thought it kind of creates a purpose for that idea.

# Part 1 : Process and designing 

## Step 1 : Finding the right piece and creating a canvas

First, I searched in google for pictures of the chess piece. I wanted to design a simple one so it doesnt take me 5 hours to print it. 
I found this one : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/chesspiece.jpg"  width="400"/>
<br>

I then proceeded by adding a canvas in Fusion360 :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/canvas.png"  width="400"/>
<br>

## Step 2 : Designing the base in 2D then 3D
<br>
Now that i have my canvas, I used the tools "line" and  "spline". 
First thing i did was to draw a vertical line that would separate the chess piece in two (the line in lightblue ). That line will be used as an symetrie axe.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/divide.png"  width="400"/>
<br>
I then took the spline tool and started drawing the base of the piece by following the lines on the canvas : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/basepiece.png"  width="400"/>
<br>
Now that i have the 2D sketch, my next step would be the turn it into a 3D object. For that, Fusion360 has a "Revolve" tool. It helps one revolve the sketch around the symetry axe. Now my base looks like this :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/baserevolved.png"  width="400"/>
<br>

## Step 2 : Designing the upper part in 2D then 3D
<br>
I used pretty much the same technique with the upper part of the piece. First I drew lines and splines over the canvas so I have the form, as you can see on the pciture.  
<br>
<img src="https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/images/revolve.png"  width="400"/>

<br>
Extruding the sketch was all I had left to do.
<br>
<img src="https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/images/sketchtoextrude.png"  width="400"/>
<img src="https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/images/extrudedupperpart.png"  width="400"/>
<br>

# Part 2 : Exporting to Ultimaker Cura and preparing to print 

After importing the file to Ultimaker Cura, these are the set ups I used : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/infill.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/shell.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/support.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/speed.png"  width="400"/>
<br>
I also chose Ultimaker Cura 5 as a printer, but I then had to change the printer to Ultimaker Cura 2+ because of technical problems. 
<br>
<br>
I needed to print supports for the face of the knight. I set up the Angle to 80 degrees so only a few support would be printed. I did't need so many after all. 
The infill layer would be of 0,1mm and the wall thickness if 0,8mm. I tried to make the wall thicker but the printing time was very long .. 1 hour 47 minutes for a wall thickness of 1mm and 1 hour 3 minutes for a wall thickness of 0,8mm. 
The infill density is set up to 10%. 
<br>
Clicking on "Splice" would then prepare the 3D Object to be printed. It calculates how long it would take the printer. It showed 1hour 13 minutes, which was still a bit long. I already brought all the parameters down so the only thing I could do was to resized the object. It was 6cm high so i changed it to 4,8cm. It brought the length down to 1hour and 3 minutes, which was fine. So I started printing.
<br>
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/prepareprinting.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/beginprinting.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/baseprinting.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/basewithinfill.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/sideview.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/almostdone.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/almostdone2.jpeg"  width="400"/>
<br>

## Download the files :

3D Fusion f3d Format :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/3D_Knight_v2.f3d)

STL  Format : 
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/3D_Knight_v2.stl)

3mf  Format : 
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/UM2_3D_Knight_v2_final.3mf)

Gcode  Format : 
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/UM2_3D_Knight_v2_final.gcode)

<br>
<br>
<br> 
This is the final verison after removing the supports : 
<br> 
<video controls src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/video.mp4"  width="400" type="video/mp4" autoplay muted loop />
<br>
<br>


