# 6. Input Devices
## Idea
I wanted to read the data of the potentiometer and translate it to a MIDI value that my DAW can understand.
Basically a Midi value is between 0 and 127, with 0 being "off" and 127 "completely on".  
<br>
I proceeded by wiring the potentiometer to the arduino like so : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/potarduino.jpeg"  width="400"/>
<br>
And now , here is the Code :
```
#define knob A0
#define led 3

void setup() { 
  pinMode(knob, INPUT);
  Serial.begin(9600);
}

void loop() {
  //Read potentimeter input value
  int value = analogRead(knob);
  value = map(value, 0, 1023, 0, 127);
  Serial.print("Pot : ");
  Serial.println(value);
  delay(100);
`
```
<br>
## Tests
So after executing the code, here are the results : 
with the potentiometer completely turned off : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/potoff.png"  width="400"/>
<br>
with the potentiometer turned on to a bit more than the half :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/pothalf.png"  width="400"/>
<br>
with the potentiometer completely turned on :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/poton.png"  width="400"/>
<br>

## Download the files :

Code :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/codearduinopottomidi.ino)

