# 8. Electronics design

## Idea
The idea behind this assignment is to design a circuit using the software Eagle.
I wanted to design a part of the circuit of my final project. I will be using 4 buttons and some potentiometers.

## Part one : Schemati Design

# Step one : Downlaod and install everything

Download Eagle and the libraries ("fab" and "FAB_Hello"). 
After the installation, I had to open the library manager and import the libraries (in the In use tabe)
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/openlibrarymanager.png"  width="400"/>
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/librairies.png"  width="400"/>
<br>
At the bottom of the page, you know the libraries were imported correctly when you see "Added Libraries (2)".

I then downloaded the arduino Mega Library for Eagle. 
The Library will be linked in the downloads part. 

# Step two : Adding the parts

I created my new project and called it "Final project".
I proceeded by adding the parts to my "workstation" by clicking on the "add parts" icon on the left.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/addparts.png"  width="100"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/add_parts.png"  width="300"/>
<br>
The parts I used are : 
- The VCC (from the fab_hello library)
- The GND (from the fab_hello library)
- Arduino Mega Board (from the arduino library)
- Buttons (Also from the fab_hello library)
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/parts.png"  width="300"/>
<br>

# Step three : The design

After having imported and added the parts I wanted to use, I started wiring them correctly. I used labels as shown in class
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/addlabel.png"  width="300"/>
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/label.png"  width="300"/>
<br>
Until now, everything was smooth and easy. The hard part was still to come. I finished wiring the parts and it looked like this : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/boardsketch.png"  width="500"/>
<br>

#Step four : Board view and wiring

By clicking on this little icon 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/boardopen.png"  width="300"/>
<br>
we get to the board view. The screen is black and the parts dont look like sketches anymore. They are the real parts now, and they are not arranged. Since it was a total mess, 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/mess.png"  width="300"/>
<br>
I rearranged them and directly started connecting them to the arduino. 
There were some things that had to be taken care of and set up. 
By clicking on the arrows at the bottom left of the screen we cann make new icons appear at the top. We then click on DRC 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/drcsetup.png"  width="300"/>
<br>
and go to the clearance tab. I had to set everything to 17mil (which is a bit over 0,4mm). 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/drc.png"  width="300"/>
<br>
Thanks to this setup, we can have a visual of when the cables can't be connected because of them crossing for example. 
<br>
By that time, I had three potentiometers. After a lot of fails, I noticed that it is impossible to connect all of them without having cables crossing.
<br>
As you can see on the next picture, I couldn't connect the GND pin to the potentiometer on the right side. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/fail2.png"  width="300"/>
<br>
Unfortunately, I had to remove one potentiometer and now everything works fine.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/workswithtwo.png"  width="300"/>
<br>

## Download the files :

The two files (schematic and board) + the arduino mega library are in this Compressed Zip file :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/project.zip)

