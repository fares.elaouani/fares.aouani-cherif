# 7. Output devices

## Idea
The goal here is to light up the LED and control the intensity of the light using the potentiometer.
<br>
As you can see, the potentiometer is connected to the LED throught the negative pole. I also used a resistance to lower the voltage.
So this is the circuit :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/pottoled.jpeg"  width="400"/>
<br>

```
#define knob 0
#define led 3


void setup() {
  pinMode(led, OUTPUT);  

}

void loop() {
  
  int val = analogRead(knob);
  val = map(val, 1, 1024, 1, 255);
  analogWrite(led, val);
}
```

## Tests
So after executing the code, here are the results (excuse me for the weak light, I did this during the day so the LED's light wasn't very intense) :
Here is the potentiometer completely off : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/potoffledoff.jpeg"  width="400"/>
<br>
Here is the potentiometer completely on
<br> : 
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/potonledon.jpeg"  width="400"/>
<br>
## Download the files :

Code :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/pottoled.ino)


