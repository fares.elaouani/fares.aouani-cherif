# 3. Laser cutting

My goal of this week was to design a box (with joints) for my hamster. 
It should be a little house with her name engraved on it.

I shall be using parametric design in Fusion 360.

# Part 1 : Design 

## Steps
These are the parameters for the box and the joints. I will explain later on why the two different kerfs.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/parameters1.png"  width="400"/>
<br>

### First step : creating the base and a wall
<br>
Here is the base. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/base.png"  width="400"/>
<br>
I first created the top joints and the left joints that I then mirrored using construction lines. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/construction.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/constlines.png"  width="400"/>
<br>
The roof and the second wall are copies of the base and the wall that I previously created. 
One can notice that the joints on the walls are different. The top and bottom ones fit to the base and the top, so they have 4 joints with a length of 20mm - kerf/2. On the other hand, there are only two joints for the front and back walls. The length is still 20mm - kerf/2.


### Second step : Front and back wall
<br>
I then proceeded by creating the front wall. As I did earlier, I just created the joints then mirrored them using construction lines.
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/frontt.png"  width="400"/>
<br>
After that, i mirrored it to create the back wall. The joints were created in the same way.
<br>

### Third step : extrude and box-up
<br>
After having created the base, front wall and left wall and having copied those, the next step was to extrude and box them up.
After some slight changes, the parts could fit perfectly. I then selected the front and back wall and clicked on add a sketch. I drew a circle and extruded it to make the two big holes that should serve as entry doors for the hamster. 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/extrudee.png"  width="400"/>
<br>

### Fourth step : Engrave the hamsters name
<br>
Last and final step of the designing part : engraving my hamster's name : vicky.
I chose the left wall.
How did I do that? Simple : I first selected the wall where I wanted the engraving to be, 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/createsketch.png"  width="400"/>
<br> 
then i clicked on "create sketch", then "add text" et voilà ! 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/addtext.png"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/vickyy.png"  width="400"/>
<br>
Last but not least, I extruded the text and gave it a deptch of 0,5mm so it doesnt pierce the wall.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/vick.png"  width="400"/>
<br>

# Part 2 : laser-cutting

##Steps

### First step : seting everything up and testing
<br>
I exported the fusion file in a DXF format and imported it to the software called "Rhinoceros 5". 
Speed was set up at 4%, Power at 100% and frequency at 19%. I sized the base and the left wall down to 1/2 of its original sizeand the next step was to focus the pointer of the laser cutter. I used a little focus tool to do that : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/focus.jpeg"  width="400"/>
<br>
Then I moved the jog to the top left corner :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/jog.jpeg"  width="400"/>
<br>
Then we started the test cut.
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/test.jpeg"  width="400"/>
<br>
The testing part was to figure out what kerf we need to take into consideration. We figured that we should use 0,5 for the base and the front wall.
Then we cut the side wall and we figured that the 0,5mm kerf was too much. The parts were really loose. That meant for us that we had to use a kerf of 0,5mm for the base's and front wall's joints, and we reduced the kerf to 0,2mm for the side wall's joints.
<br>
We had finally enough informations to finalize the set up of the laser cutter and start cutting. Here are the final parameters :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/params.jpeg"  width="400"/>
<br>
One problem occured though. When we imported the final file, we noticed that the engraving wasnt being exported with the rest. We had to create the engraving again, but this time we did it directly on Rhinoceros. I chose another font and here it is :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/engraving.jpeg"  width="400"/>
<br>
Now the cutting and the engraving could finally take place :
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/cutting.jpeg"  width="400"/>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/gravur.jpeg"  width="400"/>
<br>

# 5 minutes later ..

Everything was ready. Here is how the final result looks like : 
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/final.jpeg"  width="400"/>
<br>
<img src="https://fares.elaouani.gitlab.io/fares.aouani-cherif/images/final2.jpeg"  width="400"/>
<br>


## Download the files :

Fusion f3d Format :
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/Laser_cutting_assignment.f3d)

DXF Format : 
[Download me](https://gitlab.com/fares.elaouani/fares.aouani-cherif/-/raw/master/docs/downloads/Laser_cutting_assignment.dxf)
